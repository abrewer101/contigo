# Generated by Django 4.2.8 on 2023-12-11 21:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('words', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='words',
            name='code',
            field=models.CharField(max_length=200, unique=True),
        ),
        migrations.AlterField(
            model_name='words',
            name='name',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
