from django.shortcuts import render
from words.models import words
import random

def show_words(request):
    all_words = words.objects.all()
    random_word = random.choice(all_words)

    context = {
        "words_object": random_word,
    }
    return render(request, "words/detail.html", context)
