from django.urls import path
from words.views import show_words

urlpatterns = [
    path("words/", show_words, name="show_words"),
]
