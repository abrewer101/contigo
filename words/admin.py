from django.contrib import admin
from .models import words

@admin.register(words)
class WordsAdmin(admin.ModelAdmin):
    list_display = ("name", "id", "code")
    search_fields = ("name", "code")
    readonly_fields = ("id",)

    fieldsets = (
        ("Language Details", {
            "fields": ("name", "code")
        }),
    )
